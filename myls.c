#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <stdint.h>
#include <linux/limits.h>
#include <string.h>

void printDir(char *currentDir, int show_hidden, int long_listing);
void printPath(char *currentPath, char *itemName, int long_listing);
void getMode(char *permisions, mode_t st_mode);

#define READ 'r'
#define WRITE 'w'
#define EXEC 'x'
#define LINK 'l'
#define FOLDER 'd'

#define CURRENT_DIR "./"

int main(int argc, char *argv[]) {
    // for getopt
    int opt;
    int show_hidden = 0;
    int long_listing = 0;

    // a boolean for the case where there aren't any arguments
    int noArgs = 0;

    // pathname strings
    char *currentDir;

    // parse command line arguments for options a and l
    while ((opt = getopt(argc, argv, "al")) != -1) {
        switch (opt) {
        case 'a':
            show_hidden = 1;
            break;
        case 'l':
            long_listing = 1;
            break;
        default: /* '?' */
            fprintf(stderr, "Usage: %s [-al] [file ...] \n",
                    argv[0]);
            exit(EXIT_FAILURE);
        }
    }

    // check to see if there are no non-option arguments
    if (optind == argc) {
        noArgs = 1;
        currentDir = CURRENT_DIR;
    }

    int i = optind;
    do {
        if (!noArgs) {
            currentDir = argv[i];
        }

        // load currentDir metadata
        struct stat statbuf;

        if (lstat(currentDir, &statbuf) == -1) {
            perror("lstat");
            break;
        }

        // print metadata
        if (S_ISREG(statbuf.st_mode) || S_ISLNK(statbuf.st_mode)) {
            printPath(currentDir, currentDir, long_listing);
        }
        else if (S_ISDIR(statbuf.st_mode) || !long_listing) {
            if (optind < argc - 1) {
                printf("%s:\n", currentDir);
            }
            printDir(currentDir, show_hidden, long_listing);
        }

        // add context depended new line characters
        if (!long_listing) {
            printf("\n");
        }
        if (i < argc - 1) {
            printf("\n");
        }

        i++;

    } while(i < argc);

    exit(EXIT_SUCCESS);
}

/*
prints the information for all items in a given directory
*/
void printDir(char *currentDir, int show_hidden, int long_listing) {
    // for opendir
    struct dirent *dirPoint;
    DIR *dirP;

    dirP = opendir(currentDir);
    if (dirP == NULL) {
        perror("opendir");
        printf("Directory can not be opened '%s'\n", currentDir);
        exit(EXIT_FAILURE);
    }


    while ((dirPoint = readdir(dirP)) != NULL) {
        if (show_hidden || dirPoint->d_name[0] != '.') {
            char currentPath[PATH_MAX];
            strcpy(currentPath, currentDir);
            strcat(currentPath, "/");
            strcat(currentPath, dirPoint->d_name);
            printPath(currentPath, dirPoint->d_name, long_listing);
        }
    }

    closedir(dirP);
}

/*
prints the information for a given path
*/
void printPath(char *currentPath, char *itemName, int long_listing) {
    if (!long_listing) {
        printf("%s ", itemName);
        return;
    }

    struct stat statbuf;
    char pathType;
    char permisions[10];
    struct tm *tm;
    char dateString[256];

    if (lstat(currentPath, &statbuf) == -1) {
        perror("lstat");
        return;
    }

    // determine what type the path is
    if (S_ISDIR(statbuf.st_mode)) {
        pathType = FOLDER;
    }
    else if (S_ISREG(statbuf.st_mode)) {
        pathType = '-';
    }
    else if (S_ISLNK(statbuf.st_mode)) {
        pathType = LINK;
    }
    else {
        pathType = '!';
    }

    // get permisions
    getMode(permisions, statbuf.st_mode);

    // generate localized dateString
    tm = localtime(&statbuf.st_mtime);
    strftime(dateString, sizeof(dateString), "%b %d %H:%M", tm);

    printf("%c%s %4zu %s %s %6jd %s %s ",
            pathType,
            permisions,
            statbuf.st_nlink,
            getpwuid(statbuf.st_uid)->pw_name,
            getgrgid(statbuf.st_gid)->gr_name,
            (intmax_t)statbuf.st_size,
            dateString,
            itemName
    );
    
    // add on link info
    if (pathType == 'l') {
        char buf[PATH_MAX];
        ssize_t len;
        if ((len = readlink(currentPath, buf, sizeof(buf) - 1)) != -1) {
            buf[len] = '\0';
        }
        else {
            perror("readlink");

        }
        printf("-> %s", buf);
    }

    printf("\n");
}

/* 
takes the permisions data for a given file or directory 
    and builds a permision string 
*/
void getMode(char *permisions, mode_t st_mode) {

    permisions[0] = (st_mode & S_IRUSR) ? READ : '-';
    permisions[1] = (st_mode & S_IWUSR) ? WRITE : '-';
    permisions[2] = (st_mode & S_IXUSR) ? EXEC : '-';

    permisions[3] = (st_mode & S_IRGRP) ? READ : '-';
    permisions[4] = (st_mode & S_IWGRP) ? WRITE : '-';
    permisions[5] = (st_mode & S_IXGRP) ? EXEC : '-';

    permisions[6] = (st_mode & S_IROTH) ? READ : '-';
    permisions[7] = (st_mode & S_IWOTH) ? WRITE : '-';
    permisions[8] = (st_mode & S_IXOTH) ? EXEC : '-';

    // set null terminator
    permisions[9] = 0;
}